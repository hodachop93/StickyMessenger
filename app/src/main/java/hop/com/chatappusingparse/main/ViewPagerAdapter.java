package hop.com.chatappusingparse.main;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;

import hop.com.chatappusingparse.R;
import hop.com.chatappusingparse.people.PeopleFragment;
import hop.com.chatappusingparse.recent.RecentFragment;
import hop.com.chatappusingparse.setting.SettingFragment;
import hop.com.chatappusingparse.utils.SlidingTabLayout;

/**
 * Created by Hop on 01/09/2015.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter implements SlidingTabLayout.TabIconProvider {
    private int[] mIconResId = {
            R.drawable.ic_tab_recent,
            R.drawable.ic_tab_people,
            R.drawable.ic_tab_setting
    };

    private int mNumOfTabs;
    private Context mContext;

    public ViewPagerAdapter(AppCompatActivity activity, int mNumOfTabs) {
        super(activity.getSupportFragmentManager());
        this.mContext = activity;
        this.mNumOfTabs = mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new RecentFragment();
            case 1:
                return new PeopleFragment();
            case 2:
                return new SettingFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public int getPageIconResId(int position) {
        return mIconResId[position];
    }
}
