package hop.com.chatappusingparse.main;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import hop.com.chatappusingparse.R;
import hop.com.chatappusingparse.model.User;
import hop.com.chatappusingparse.utils.SlidingTabLayout;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    //private Toolbar mToolbar;
    private ViewPager mViewPager;
    public static User sUser;

    private static final int NUM_OF_TABS_VIEW_PAGER = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        initViewPager();

    }

    private void initViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(this, NUM_OF_TABS_VIEW_PAGER);
        mViewPager.setAdapter(adapter);
        mViewPager.setCurrentItem(1);
        SlidingTabLayout slidingTabs = (SlidingTabLayout) findViewById(R.id.slidingTabs);
        slidingTabs.setCustomTabView(R.layout.img_title_viewpager, R.id.img_title);
        slidingTabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white);
            }
        });
        slidingTabs.setViewPager(mViewPager);
    }


}
