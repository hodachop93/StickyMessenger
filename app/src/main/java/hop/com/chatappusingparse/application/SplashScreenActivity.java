package hop.com.chatappusingparse.application;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.gson.Gson;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.net.UnknownServiceException;

import hop.com.chatappusingparse.R;
import hop.com.chatappusingparse.login.LoginActivity;
import hop.com.chatappusingparse.main.MainActivity;
import hop.com.chatappusingparse.model.User;
import hop.com.chatappusingparse.people.PeopleFragment;
import hop.com.chatappusingparse.utils.Utils;

/**
 * Created by Hop on 28/08/2015.
 */
public class SplashScreenActivity extends AppCompatActivity {
    private static final String TAG = SplashScreenActivity.class.getSimpleName();
    private SharedPreferences mSharedPref;
    private boolean isLoggedIn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mSharedPref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF_NAME, MODE_PRIVATE);

        isLoggedIn = checkLoggedIn();

        if (isLoggedIn) {
            new LoginAsyncTask().execute();
        } else {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, Config.SPLASH_TIME_OUT);
        }

    }

    private boolean checkLoggedIn() {
        String username = mSharedPref.getString(Config.KEY_SHARED_PREF_USERNAME, null);
        if (username == null) {
            return false;
        } else {
            return true;
        }
    }

    private class LoginAsyncTask extends AsyncTask<Void, Void, Void> {
        private User user;

        @Override
        protected Void doInBackground(Void... params) {
            String username = mSharedPref.getString(Config.KEY_SHARED_PREF_USERNAME, "");
            String password = mSharedPref.getString(Config.KEY_SHARED_PREF_PASSWORD, "");

            ParseUser.logInInBackground(username, password,
                    new LogInCallback() {
                        @Override
                        public void done(ParseUser parseUser, ParseException e) {
                            if (parseUser != null) {
                                //Login successful
                                isLoggedIn = true;
                                user = (User) parseUser;

                                Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);

                                MainActivity.sUser = user;
                                startActivity(intent);
                                finish();
                            } else {
                                //Login fail
                                isLoggedIn = false;
                                e.printStackTrace();
                                Log.i("Login", "Loggin fail");
                            }
                        }
                    });
            return null;
        }


    }

}
