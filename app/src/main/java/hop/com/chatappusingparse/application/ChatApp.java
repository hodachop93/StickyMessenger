package hop.com.chatappusingparse.application;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import hop.com.chatappusingparse.model.User;

/**
 * Created by Hop on 08/09/2015.
 */
public class ChatApp extends Application{
    @Override
    public void onCreate() {
        super.onCreate();

        ParseObject.registerSubclass(User.class);
        Parse.initialize(this, Config.APPLICATION_KEY, Config.CLIENT_KEY);
    }

}
