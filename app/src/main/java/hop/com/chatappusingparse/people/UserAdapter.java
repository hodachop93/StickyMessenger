package hop.com.chatappusingparse.people;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseFile;

import java.util.List;

import hop.com.chatappusingparse.R;
import hop.com.chatappusingparse.model.User;
import hop.com.chatappusingparse.utils.CircleImageView;

/**
 * Created by Hop on 30/09/2015.
 */
public class UserAdapter extends BaseAdapter {
    private Context mContext;
    private List<User> userList;

    public UserAdapter(Context mContext, List<User> userList) {
        this.mContext = mContext;
        this.userList = userList;
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int position) {
        return userList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_person, parent, false);
            viewHolder = new ViewHolder();

            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.avatar = (CircleImageView) convertView.findViewById(R.id.avatar);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        setValue(viewHolder, position);
        return convertView;
    }

    private void setValue(ViewHolder viewHolder, int position) {
        User user = userList.get(position);
        long start = System.currentTimeMillis();
        ParseFile parseFile = user.getAvatarPhoto();


        /*byte[] bytes = null;
        try {
           bytes  = parseFile.getData();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Bitmap bmp = null;
        if(bytes!=null){
           bmp  = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        }*/
        long end = System.currentTimeMillis();
        long seconds = (end-start);
        viewHolder.name.setText(user.getName());
        /*if(bmp!=null){
            viewHolder.avatar.setImageBitmap(bmp);
        }*/

        viewHolder.name.setText(String.valueOf(seconds));
    }

    private static class ViewHolder {
        TextView name;
        CircleImageView avatar;
    }
}
