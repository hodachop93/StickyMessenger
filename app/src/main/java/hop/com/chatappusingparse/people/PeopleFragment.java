package hop.com.chatappusingparse.people;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.util.List;

import hop.com.chatappusingparse.R;
import hop.com.chatappusingparse.application.Config;
import hop.com.chatappusingparse.chat.ChatActivity;
import hop.com.chatappusingparse.main.MainActivity;
import hop.com.chatappusingparse.model.User;
import hop.com.chatappusingparse.model.ParseContract.UserEntry;
import hop.com.chatappusingparse.utils.Utils;

/**
 * Created by Hop on 01/09/2015.
 */
public class PeopleFragment extends Fragment {
    private List<User> userList;
    private Context mContext;
    private UserAdapter mAdapter;
    ListView lvPeople;

    private boolean isUserListLoaded;
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Restore State Here
        if (savedInstanceState!=null){
            isUserListLoaded = savedInstanceState.getBoolean("UserListIsLoaded", false);
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save State Here
        outState.putBoolean("UserListIsLoaded", true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        updateUserStatus(true);
        this.mContext = getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_people, container, false);
        lvPeople = (ListView) rootView.findViewById(R.id.lvPeople);

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!isUserListLoaded) {
            loadUserList();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        updateUserStatus(false);
    }

    private void loadUserList() {
        //final ProgressDialog dialog = ProgressDialog.show(mContext, null, getString(R.string.message_loading));
        User.getQuery().whereNotEqualTo(UserEntry.COLUMN_USERNAME,
                MainActivity.sUser.getUsername())
                .findInBackground(new FindCallback<ParseUser>() {
                    @Override
                    public void done(List<ParseUser> list, ParseException e) {
                        //dialog.dismiss();
                        if (list != null) {
                            if (list.size() == 0) {
                                Toast.makeText(mContext, getString(R.string.message_no_users_chat),
                                        Toast.LENGTH_SHORT).show();
                                return;
                            }

                            userList = (List<User>) (List<?>) list;
                            mAdapter = new UserAdapter(mContext, userList);
                            lvPeople.setAdapter(mAdapter);
                            lvPeople.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    startActivity(new Intent(getActivity(), ChatActivity.class)
                                            .putExtra(Config.KEY_EXTRA_DATA, userList.get(position).getName()));
                                }
                            });

                        } else {
                            Utils.showDialog(
                                    mContext,
                                    getString(R.string.err_users) + " "
                                            + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                });
    }

    /**
     * Update user status.
     *
     * @param online true if user is online
     */
    private void updateUserStatus(boolean online) {
        MainActivity.sUser.put(UserEntry.COLUMN_ONLINE, online);
        User user = new User();
        MainActivity.sUser.saveEventually();
    }

}
