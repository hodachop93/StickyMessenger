package hop.com.chatappusingparse.custom;

import android.support.v4.app.Fragment;
import android.view.View;

import hop.com.chatappusingparse.utils.TouchEffect;

/**
 * Created by Hop on 04/10/2015.
 */
public class CustomFragment extends Fragment implements View.OnClickListener{
    private static final TouchEffect touchEffect = new TouchEffect();

    /**
     * Set the touch and click listener for a view with a given id
     *
     * @param v The view we want set touch and click
     * @return The view on which the listener applied
     */
    public View setTouchAndClick(View v) {
        setClick(v);
        if (v != null) {
            v.setOnTouchListener(touchEffect);
        }
        return v;
    }

    /**
     * Set the click listener for a view with a given id
     *
     * @param v The view we want set touch and click
     * @return The view on which the listener applied
     */
    private View setClick(View v) {
        if (v != null) {
            v.setOnClickListener(this);
        }
        return v;
    }

    @Override
    public void onClick(View v) {

    }
}
