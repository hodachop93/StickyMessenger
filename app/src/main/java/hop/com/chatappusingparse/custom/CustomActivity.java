package hop.com.chatappusingparse.custom;

import android.support.v7.app.AppCompatActivity;
import android.view.View;

import hop.com.chatappusingparse.utils.TouchEffect;

/**
 * Created by Hop on 18/09/2015.
 */
public class CustomActivity extends AppCompatActivity implements View.OnClickListener {
    private static final TouchEffect touchEffect = new TouchEffect();

    /**
     * Set the touch and click listener for a view with a given id
     *
     * @param id The id of the view
     * @return The view on which the listener applied
     */
    public View setTouchAndClick(int id) {
        View v = setClick(id);
        if (v != null) {
            v.setOnTouchListener(touchEffect);
        }
        return v;
    }

    /**
     * Set the click listener for a view with a given id
     *
     * @param id The id of the view
     * @return The view on which the listener applied
     */
    private View setClick(int id) {
        View v = findViewById(id);
        if (v != null) {
            v.setOnClickListener(this);
        }
        return v;
    }

    @Override
    public void onClick(View v) {

    }
}
