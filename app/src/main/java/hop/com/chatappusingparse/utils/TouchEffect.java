package hop.com.chatappusingparse.utils;

import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Hop on 18/09/2015.
 * The class TouchEffect is the implementation of OnTouchListener interface. We
 * can apply this to views mostly Buttons to provide Touch effect and that view
 * must have a valid background. The current implementation simply set Alpha value
 * of View background
 */
public class TouchEffect implements View.OnTouchListener {
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            Drawable d = v.getBackground();
            d.mutate();
            d.setAlpha(150);
            v.setBackgroundDrawable(d);
        } else if (event.getAction() == MotionEvent.ACTION_UP ||
                event.getAction() == MotionEvent.ACTION_CANCEL) {
            Drawable d = v.getBackground();
            //d.mutate();
            d.setAlpha(255);
            v.setBackgroundDrawable(d);
        }
        return false;
    }
}
