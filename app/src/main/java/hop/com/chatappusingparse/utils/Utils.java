package hop.com.chatappusingparse.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import hop.com.chatappusingparse.R;

/**
 * Utils class have sever common public static methods such as: show alert dialog, ....
 * in order to help you save up your time to code
 * Created by Hop on 25/09/2015.
 */
public class Utils {
    public static AlertDialog showDialog(Context ctx, int msg) {
        return showDialog(ctx, ctx.getString(msg));
    }

    public static AlertDialog showDialog(Context ctx, String msg) {
        return showDialog(ctx, msg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    public static AlertDialog showDialog(Context ctx, String msg, DialogInterface.OnClickListener listener) {
        return showDialog(ctx, msg, ctx.getString(R.string.ok), null, listener, null);
    }

    public static AlertDialog showDialog(Context ctx, String msg, String btnPos, String btnNeg,
                                         DialogInterface.OnClickListener listenerPos,
                                         DialogInterface.OnClickListener listenerNeg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setMessage(msg).setCancelable(false)
                .setPositiveButton(btnPos, listenerPos);
        if (btnNeg != null && listenerNeg != null) {
            builder.setNegativeButton(btnNeg, listenerNeg);
        }
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }
}
