package hop.com.chatappusingparse.utils;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import hop.com.chatappusingparse.application.Config;

/**
 * Created by Hop on 25/09/2015.
 */
public class ParseUtils {
    private static final String TAG = ParseUtils.class.getSimpleName();

    public static void verifyParseConfiguration(Context context){
        if(TextUtils.isEmpty(Config.APPLICATION_KEY) || TextUtils.isEmpty(Config.CLIENT_KEY)){
            Toast.makeText(context, "Please configure your Parse Application ID and Client Key in AppConfig.java",
                    Toast.LENGTH_LONG).show();
            ((AppCompatActivity) context).finish();
        }
    }

    public static void registerParse(Context context) {
        // initializing parse library
        Parse.initialize(context, Config.APPLICATION_KEY, Config.CLIENT_KEY);
        ParseInstallation.getCurrentInstallation().saveInBackground();

        ParsePush.subscribeInBackground(Config.PARSE_CHANNEL, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Log.e(TAG, "Successfully subscribed to Parse!");
            }
        });
    }

    public static void subscribeWithEmail(String email) {
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();

        installation.put("email", email);

        installation.saveInBackground();
    }
}
