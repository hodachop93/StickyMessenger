package hop.com.chatappusingparse.setting;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import hop.com.chatappusingparse.R;
import hop.com.chatappusingparse.application.Config;
import hop.com.chatappusingparse.custom.CustomFragment;
import hop.com.chatappusingparse.main.MainActivity;

/**
 * Created by Hop on 12/09/2015.
 */
public class SettingFragment extends CustomFragment {
    Button btnLogout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_setting, container, false);
        btnLogout = (Button) rootView.findViewById(R.id.btnLogout);
        setTouchAndClick(btnLogout);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int id = v.getId();
        if (id == R.id.btnLogout) {
            Context context = getContext();
            SharedPreferences sharedPref = context.getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();

            editor.remove(Config.KEY_SHARED_PREF_USERNAME);
            editor.remove(Config.KEY_SHARED_PREF_PASSWORD);
            editor.commit();

            MainActivity.sUser = null;
        }
    }
}
