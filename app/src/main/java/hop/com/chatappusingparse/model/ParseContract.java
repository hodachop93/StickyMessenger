package hop.com.chatappusingparse.model;

/**
 * Created by Hop on 30/09/2015.
 */
public class ParseContract {
    public class UserEntry {
        public static final String TABLE_NAME = "User";
        public static final String COLUMN_USERNAME = "username";
        public static final String COLUMN_PASSWORD = "password";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_AVATAR = "avatarPhoto";
        public static final String COLUMN_ONLINE = "online";
    }
}
