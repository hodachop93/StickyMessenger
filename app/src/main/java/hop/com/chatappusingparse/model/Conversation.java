package hop.com.chatappusingparse.model;

import java.util.Date;

import hop.com.chatappusingparse.main.MainActivity;

/**
 * Created by Hop on 18/09/2015.
 * The class Conversation is a java bean class represents a single chat
 * conversation message
 */
public class Conversation {
    public static final int STATUS_SENDING=0;
    public static final int STATUS_SENT=1;
    public static final int STATUS_FAILED=2;
    private String msg;
    private int status = STATUS_SENT;
    private Date date;
    private String sender;

    public Conversation() {
    }

    public Conversation(String msg, Date date, String sender) {
        this.msg = msg;
        this.date = date;
        this.sender = sender;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * Check if it is sent
     * @return true if it is sent
     */
    public boolean isSent(){
        return MainActivity.sUser.getUsername().equals(sender);
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
