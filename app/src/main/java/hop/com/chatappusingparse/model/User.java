package hop.com.chatappusingparse.model;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseUser;


import hop.com.chatappusingparse.model.ParseContract.UserEntry;

/**
 * Created by Hop on 18/09/2015.
 */
@ParseClassName("_User")
public class User extends ParseUser {


    public String getName() {
        return this.getString(UserEntry.COLUMN_NAME);
    }

    public void setName(String name) {
        this.put(UserEntry.COLUMN_NAME, name);
    }

    public ParseFile getAvatarPhoto() {
        return this.getParseFile(UserEntry.COLUMN_AVATAR);
    }

}
