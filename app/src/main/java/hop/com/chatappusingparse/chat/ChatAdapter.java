package hop.com.chatappusingparse.chat;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import hop.com.chatappusingparse.R;
import hop.com.chatappusingparse.model.Conversation;

/**
 * Created by Hop on 27/10/2015.
 */
public class ChatAdapter extends BaseAdapter {
    private static final int VIEW_TYPE_COUNT = 2;
    private static final int VIEW_TYPE_SENT = 0;
    private static final int VIEW_TYPE_RECEIVED = 1;

    private List<Conversation> mConvList;
    private Context mContext;

    public ChatAdapter(List<Conversation> mConvList, Context mContext) {
        this.mConvList = mConvList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mConvList.size();
    }

    @Override
    public int getViewTypeCount() {
        return VIEW_TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        Conversation c = getItem(position);
        boolean isSent = c.isSent();
        if (isSent) {
            return VIEW_TYPE_SENT;
        } else {
            return VIEW_TYPE_RECEIVED;
        }
    }

    @Override
    public Conversation getItem(int position) {
        return mConvList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        TextView tvTime, tvMessage, tvStatus;

        public ViewHolder(View view) {
            tvTime = (TextView) view.findViewById(R.id.tvTime);
            tvMessage = (TextView) view.findViewById(R.id.tvMessage);
            tvStatus = (TextView) view.findViewById(R.id.tvStatus);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        int viewType = getItemViewType(position);
        int layoutId = -1;
        switch (viewType) {
            case VIEW_TYPE_SENT:
                layoutId = R.layout.chat_item_sent;
                break;
            case VIEW_TYPE_RECEIVED:
                layoutId = R.layout.chat_item_received;
                break;
        }

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(layoutId, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        setValue(viewHolder, position);
        return convertView;
    }

    private void setValue(ViewHolder viewHolder, int position) {
        Conversation c = getItem(position);

        CharSequence time = DateUtils.getRelativeDateTimeString(mContext, c.getDate().getTime(), DateUtils.SECOND_IN_MILLIS, DateUtils.DAY_IN_MILLIS, 0);
        viewHolder.tvTime.setText(time);

        viewHolder.tvMessage.setText(c.getMsg());
        
        String status;
        if (c.isSent()) {
            switch (c.getStatus()) {
                case Conversation.STATUS_FAILED:
                    status = "Failed";
                    break;
                case Conversation.STATUS_SENDING:
                    status = "Sending";
                    break;
                case Conversation.STATUS_SENT:
                    status = "Delivered";
                    break;
                default:
                    status = "";
            }
        } else {
            status = "Not sent";
        }
        viewHolder.tvStatus.setText(status);
    }
}
