package hop.com.chatappusingparse.chat;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hop.com.chatappusingparse.R;
import hop.com.chatappusingparse.application.Config;
import hop.com.chatappusingparse.custom.CustomActivity;
import hop.com.chatappusingparse.main.MainActivity;
import hop.com.chatappusingparse.model.Conversation;

/**
 * Created by Hop on 08/10/2015.
 */
public class ChatActivity extends CustomActivity implements View.OnClickListener,
        TextWatcher {
    Toolbar toolbar;
    EditText edtMesageBox;
    Button btnSend;
    ListView lvChat;
    private List<Conversation> mConversationList;
    private ChatAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getIntent().getStringExtra(Config.KEY_EXTRA_DATA));
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavUtils.navigateUpFromSameTask(ChatActivity.this);
            }
        });
        setSupportActionBar(toolbar);
        lvChat = (ListView) findViewById(R.id.lvChat);
        edtMesageBox = (EditText) findViewById(R.id.edtMessageBox);
        btnSend = (Button) findViewById(R.id.btnSend);

        mAdapter=new ChatAdapter(mConversationList, this);
        edtMesageBox.addTextChangedListener(this);
        mConversationList = new ArrayList<>();
        setTouchAndClick(R.id.btnSend);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int id = v.getId();
        if (id == R.id.btnSend) {
            sendMessage();
        }
    }

    private void sendMessage() {
        if (edtMesageBox.length() == 0)
            return;

        String message = edtMesageBox.getText().toString();
        Conversation c = new Conversation(message, new Date(), MainActivity.sUser.getUsername());
        
        c.setStatus(Conversation.STATUS_SENDING);
        mConversationList.add(c);
        mAdapter.notifyDataSetChanged();
        edtMesageBox.setText(null);

        ParseObject po = new ParseObject("Chat");
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (edtMesageBox.length() != 0) {
            btnSend.setEnabled(true);
        } else {
            btnSend.setEnabled(false);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
