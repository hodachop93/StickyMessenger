package hop.com.chatappusingparse.login;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import hop.com.chatappusingparse.R;
import hop.com.chatappusingparse.custom.CustomActivity;
import hop.com.chatappusingparse.model.User;
import hop.com.chatappusingparse.utils.Utils;

/**
 * Created by Hop on 18/09/2015.
 */
public class RegisterActivity extends CustomActivity {
    private EditText mEdtName;
    private EditText mEdtPass;
    private EditText mEdtPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mEdtName = (EditText) findViewById(R.id.edtName);
        mEdtPass = (EditText) findViewById(R.id.edtPassword);
        mEdtPhone = (EditText) findViewById(R.id.edtPhone);

        setTouchAndClick(R.id.btnRegister);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        String name = mEdtName.getText().toString();
        String pass = mEdtPass.getText().toString();
        String username = mEdtPhone.getText().toString();

        if (name.equals("") ||  pass.equals("") ||  username.equals("")) {
            Utils.showDialog(this, R.string.err_empty_fields);
            return;
        }
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.pro_dialog_message));
        progressDialog.show();

        User user = new User();
        user.setUsername(username);
        user.setPassword(pass);
        user.setName(name);

        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                progressDialog.dismiss();
                if (e == null) {
                    setResult(LoginActivity.RESULT_CODE_SIGNUP);
                    finish();
                } else {
                    Utils.showDialog(RegisterActivity.this,
                            getString(R.string.err_register_account) + " " +
                                    e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }
}
