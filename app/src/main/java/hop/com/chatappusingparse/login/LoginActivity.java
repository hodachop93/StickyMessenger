package hop.com.chatappusingparse.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import hop.com.chatappusingparse.R;
import hop.com.chatappusingparse.application.Config;
import hop.com.chatappusingparse.custom.CustomActivity;
import hop.com.chatappusingparse.main.MainActivity;
import hop.com.chatappusingparse.model.User;
import hop.com.chatappusingparse.people.PeopleFragment;
import hop.com.chatappusingparse.utils.Utils;

/**
 * Created by Hop on 28/08/2015.
 */
public class LoginActivity extends CustomActivity {
    public static final int RESULT_CODE_SIGNUP = 100;
    EditText edtPhone;
    EditText edtPassword;

    private SharedPreferences mSharedPref;
    private SharedPreferences.Editor mEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mSharedPref = getSharedPreferences(Config.SHARED_PREF_NAME, MODE_PRIVATE);
        mEditor = mSharedPref.edit();

        edtPhone = (EditText) findViewById(R.id.edtPhone);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        setAction();
    }

    private void setAction() {
        setTouchAndClick(R.id.btnLogin);
        setTouchAndClick(R.id.btnSignUp);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (v.getId() == R.id.btnLogin) {
            String username = edtPhone.getText().toString();
            String password = edtPassword.getText().toString();
            if (username.equals("") || password.equals("")) {
                Utils.showDialog(this, R.string.err_empty_fields);
                return;
            } else {
                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setMessage(getString(R.string.pro_dialog_message));
                progressDialog.show();

                ParseUser.logInInBackground(username, password,
                        new LogInCallback() {
                            @Override
                            public void done(ParseUser parseUser, ParseException e) {
                                progressDialog.dismiss();
                                if (parseUser != null) {
                                    //Login successful
                                    doLoginSuccessful(parseUser);
                                } else {
                                    //Login fail
                                    Utils.showDialog(LoginActivity.this,
                                            getString(R.string.err_login) + " " + e.getMessage());
                                    e.printStackTrace();
                                }
                            }
                        });
            }


        } else if (v.getId() == R.id.btnSignUp) {
            Intent intent = new Intent(this, RegisterActivity.class);
            startActivityForResult(intent, RESULT_CODE_SIGNUP);
        }
    }

    private void doLoginSuccessful(ParseUser parseUser) {
        putUserToSharedPref(parseUser);
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);

        MainActivity.sUser = (User) parseUser;
        startActivity(intent);
        finish();
    }

    private void putUserToSharedPref(ParseUser parseUser) {
        String username = parseUser.getUsername();
        String password = edtPassword.getText().toString();
        mEditor.putString(Config.KEY_SHARED_PREF_USERNAME, username);
        mEditor.putString(Config.KEY_SHARED_PREF_PASSWORD, password);

        mEditor.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /*@Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }*/
}
